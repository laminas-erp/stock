# Laminas Module lerp/stock

[Lingana ERP](https://linganaerp.de)

Im stock steht immer die quantityunit_uuid mit quantityunit_resolution = 1

stockin_quantity_resolution nutzt alleine in `view_stockin` nichts, weil die Basiseinheit fehlt.
Die muss/müsste dann später dazu geklebt werden.

Also, quantityunit_resolution immer = 1 im stock!

Der `stock` kann sich im Service das assoc array der Quantityunits bereit halten.
