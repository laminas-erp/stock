<?php

namespace Lerp\Stock\Controller\Ajax;

use Bitkorn\User\Service\UserService;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ProductStockAjaxControllerTest extends AbstractHttpControllerTestCase
{
    protected function setUp(): void
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
        // Grabbing the full application configuration:
            include __DIR__ . '/../../../../../../config/application.config.php',
            $configOverrides
        ));
        parent::setUp();

        /** @var UserService $userService */
        $userService = $this->getApplication()->getServiceManager()->get(UserService::class);
        $userService->setSessionHashManually('151956ad612953e61533b9d2d5844d65df5b5c8c4e35f5e49375ef77711b6676');
    }

    public function testIndexActionCanBeAccessed()
    {
        try {
            $this->dispatch('/lerp-stock-ajax-product-stockinview-location', 'POST'
                , ['product_uuid'           => ['16294469-f531-40d6-89e9-3ab677a47c45']
                    , 'location_place_uuid' => '0a6cea79-9869-4da4-b8db-02bd21d1488a']);
        } catch (\Exception $exception) {
            var_dump($exception);
        }
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('lerp');
        $this->assertControllerName(ProductStockAjaxController::class);
        $this->assertControllerClass('ProductStockAjaxController');
        $this->assertMatchedRouteName('lerp_stock_ajax_productstock_stockinviewlocation');
    }
}
