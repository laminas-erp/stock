<?php

namespace Lerp\Stock\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;

class StockChargeTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'stock_charge';

    /**
     * @return array
     */
    public function getStockChargeUuidAssoc(): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                foreach ($result->toArray() as $row) {
                    $assoc[$row['stock_charge_uuid']] = $row['stock_charge_code'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }

    public function existStockChargeCode(string $chargeCode): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['stock_charge_code' => $chargeCode]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param string $chargeCode Case insensitive.
     * @return array
     */
    public function searchStockCharge(string $chargeCode): array
    {
        $select = $this->sql->select();
        try {
            $chargeCode = strtolower($chargeCode);
            $select->where->like(new Expression('LOWER(stock_charge_code)'), "%$chargeCode%");
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $chargeCode
     * @return string
     */
    public function getStockChargeUuidByChargeCode(string $chargeCode): string
    {
        $select = $this->sql->select();
        try {
            $select->where(['stock_charge_code' => $chargeCode]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0]['stock_charge_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertStockCharge(string $stockinChargeCode, string $productUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'stock_charge_uuid' => $uuid,
                'stock_charge_code' => $stockinChargeCode,
                'product_uuid'      => $productUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
