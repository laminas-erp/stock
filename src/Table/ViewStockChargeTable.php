<?php

namespace Lerp\Stock\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewStockChargeTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_stock_charge';

    /**
     * @param string $productUuid
     * @return array
     */
    public function getStockChargesForProduct(string $productUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
