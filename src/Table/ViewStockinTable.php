<?php

namespace Lerp\Stock\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Stock\Entity\ParamsStockinSearchEntity;

class ViewStockinTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_stockin';

    public function getStockin(string $stockinUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['stockin_uuid' => $stockinUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @return float
     */
    public function getStockQuantityForProduct(string $productUuid): float
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                'sum_qntty_in'  => new Expression('SUM(stockin_quantity)'),
                'sum_qntty_out' => new Expression('SUM(stockout_quantity)')
            ]);
            $select->where(['product_uuid' => $productUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                $curr = $result->current()->getArrayCopy();
                return $curr['sum_qntty_in'] - $curr['sum_qntty_out'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getProductStocks(string $productUuid, bool $alsoEmpty = false): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid]);
            if (!$alsoEmpty) {
                $select->where->lessThan('stockout_quantity', new Expression('stockin_quantity'));
            }
            $select->order('stockin_time_create DESC');
            $q = $select->getSqlString($this->adapter->platform);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getProductStockinsForLocation(string $productUuid, string $locationPlaceUuid, bool $noneEmptyStock = true): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid, 'location_place_uuid' => $locationPlaceUuid]);
            $select->order('stockin_time_create ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function searchProductStocks(ParamsStockinSearchEntity $queryParamsStockinEntity, bool $doCount = false): array
    {
        $select = $this->sql->select();
        try {
            if ($doCount) {
                $select->columns(['count_stockins' => new Expression('COUNT(stockin_uuid)')]);
                $queryParamsStockinEntity->setDoCount(true);
            }
            $queryParamsStockinEntity->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                if (!$doCount) {
                    return $arr;
                } else {
                    return [$arr[0]['count_stockins']];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
