<?php

namespace Lerp\Stock\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Stock\Entity\ParamsStockoutEntity;

class StockoutTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'stockout';

    /**
     * @param string $stockoutUuid
     * @return array
     */
    public function getStockout(string $stockoutUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['stockout_uuid' => $stockoutUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $stockinUuid
     * @param float $ouantity
     * @param string $comment
     * @param string $userUuid
     * @return string
     */
    public function insertStockoutFree(string $stockinUuid, float $ouantity, string $comment, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'stockout_uuid' => $uuid,
                'stockin_uuid' => $stockinUuid,
                'stockout_quantity' => $ouantity,
                'user_uuid_create' => $userUuid,
                'stockout_comment' => $comment,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param ParamsStockoutEntity $ppse
     * @return string
     */
    public function insertStockoutEntity(ParamsStockoutEntity $ppse): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $ppse->computeInsert($insert, $uuid);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
