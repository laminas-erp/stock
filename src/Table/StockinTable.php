<?php

namespace Lerp\Stock\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Stock\Entity\StockinEntity;

class StockinTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'stockin';

    /**
     * @param string $stockinUuid
     * @return array
     */
    public function getStockin(string $stockinUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['stockin_uuid' => $stockinUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getStockinsByChargeCode(string $chargeCode): array
    {
        $select = $this->sql->select();
        try {
            $selectCharge = new Select('stock_charge');
            $selectCharge->columns(['stock_charge_uuid']);
            $selectCharge->where(['stock_charge_code' => $chargeCode]);

            $select->where->in('stock_charge_uuid', $selectCharge);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getStockinsByChargeUuid(string $chargeUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['stock_charge_uuid' => $chargeUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $productUuid
     * @param float $quantity
     * @param string $quantityUnitUuid
     * @param string $locationCaseUuid
     * @param string $chargeUuid
     * @param string $serialNo
     * @param string $timeApprove
     * @param string $userUuid
     * @param string $comment
     * @param string $factoryOrderUuid
     * @param string $purchaseOrderItemUuid
     * @return string
     */
    public function insertStockin(string $productUuid, float $quantity, string $quantityUnitUuid, string $locationCaseUuid
        , string                         $chargeUuid, string $serialNo, string $timeApprove, string $userUuid, string $comment, string $factoryOrderUuid = '', string $purchaseOrderItemUuid = ''): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'stockin_uuid'             => $uuid,
                'product_uuid'             => $productUuid,
                'stockin_quantity'         => $quantity,
                'quantityunit_uuid'        => $quantityUnitUuid,
                'stock_charge_uuid'        => $chargeUuid ?: null,
                'factoryorder_uuid'        => $factoryOrderUuid ?: null,
                'purchase_order_item_uuid' => $purchaseOrderItemUuid ?: null,
                'stockin_serial_no'        => $serialNo ?: null,
                'stockin_time_approve'     => $timeApprove ?: null,
                'user_uuid_create'         => $userUuid,
                'stockin_comment'          => $comment,
                'location_case_uuid'       => $locationCaseUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertStockinWithEntity(StockinEntity $stockinEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'stockin_uuid'             => $uuid,
                'product_uuid'             => $stockinEntity->getProductUuid(),
                'stockin_quantity'         => $stockinEntity->getStockinQuantity(),
                'quantityunit_uuid'        => $stockinEntity->getQuantityunitUuid(),
                'stockin_charge_supplier'  => $stockinEntity->getStockChargeSupplier() ?: null,
                'stock_charge_uuid'        => $stockinEntity->getStockChargeUuid() ?: null,
                'factoryorder_uuid'        => $stockinEntity->getFactoryorderUuid() ?: null,
                'purchase_order_item_uuid' => $stockinEntity->getPurchaseOrderItemUuid() ?: null,
                'stockin_serial_no'        => $stockinEntity->getStockinSerialNo() ?: null,
                'stockin_time_approve'     => $stockinEntity->getStockinTimeApprove() ?: null,
                'user_uuid_create'         => $stockinEntity->getUserUuidCreate(),
                'stockin_comment'          => $stockinEntity->getStockinComment(),
                'location_case_uuid'       => $stockinEntity->getLocationCaseUuid(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function existPairingStockinUuidChargeUuid(string $stockinUuid, string $chargeUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['stockin_uuid' => $stockinUuid, 'stock_charge_uuid' => $chargeUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getStockinsForProductAndLocationCase(string $productUuid, string $locationCaseUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['product_uuid' => $productUuid, 'location_case_uuid' => $locationCaseUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
