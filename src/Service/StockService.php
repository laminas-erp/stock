<?php

namespace Lerp\Stock\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Stdlib\ArrayUtils;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Stock\Entity\ParamsStockoutEntity;
use Lerp\Stock\Entity\ParamsStockinSearchEntity;
use Lerp\Stock\Entity\StockinEntity;
use Lerp\Stock\Table\StockChargeTable;
use Lerp\Stock\Table\StockinTable;
use Lerp\Stock\Table\StockoutTable;
use Lerp\Stock\Table\ViewProductStockTable;
use Lerp\Stock\Table\ViewStockChargeTable;
use Lerp\Stock\Table\ViewStockinTable;
use Lerp\Stock\Table\ViewStockoutTable;

class StockService extends AbstractService
{
    const STOCKIN_ORIGIN_FACTORYORDER = 1;
    const STOCKIN_ORIGIN_PURCHASEORDER = 2;
    public static array $stockinOrigins = [
        self::STOCKIN_ORIGIN_FACTORYORDER  => 'factoryorder',
        self::STOCKIN_ORIGIN_PURCHASEORDER => 'purchase_order',
    ];

    protected StockinTable $stockinTable;
    protected StockoutTable $stockoutTable;
    protected ViewStockinTable $viewStockinTable;
    protected ViewStockoutTable $viewStockoutTable;
    protected ViewProductStockTable $viewProductStockTable;
    protected StockChargeTable $stockChargeTable;
    protected ViewStockChargeTable $viewStockChargeTable;
    protected QuantityUnitService $quantityUnitService;

    public function setStockinTable(StockinTable $stockinTable): void
    {
        $this->stockinTable = $stockinTable;
    }

    public function setStockoutTable(StockoutTable $stockoutTable): void
    {
        $this->stockoutTable = $stockoutTable;
    }

    public function setViewStockinTable(ViewStockinTable $viewStockinTable): void
    {
        $this->viewStockinTable = $viewStockinTable;
    }

    public function setViewStockoutTable(ViewStockoutTable $viewStockoutTable): void
    {
        $this->viewStockoutTable = $viewStockoutTable;
    }

    public function setViewProductStockTable(ViewProductStockTable $viewProductStockTable): void
    {
        $this->viewProductStockTable = $viewProductStockTable;
    }

    public function setStockChargeTable(StockChargeTable $stockChargeTable): void
    {
        $this->stockChargeTable = $stockChargeTable;
    }

    public function setViewStockChargeTable(ViewStockChargeTable $viewStockChargeTable): void
    {
        $this->viewStockChargeTable = $viewStockChargeTable;
    }

    public function setQuantityUnitService(QuantityUnitService $quantityUnitService): void
    {
        $this->quantityUnitService = $quantityUnitService;
    }

    /**
     * Its try to fetch the 'stock_charge_uuid' with the 'charge_code'.
     *
     * @param string $productUuid
     * @param float $quantity
     * @param string $quantityUnitUuid
     * @param string $locationCaseUuid
     * @param string $stockChargeUuid INSERT with 'charge_uuid' only if $chargeCode exist.
     * @param string $serialNo
     * @param string $timeApprove
     * @param string $userUuid
     * @param string $comment
     * @param string $factoryOrderUuid
     * @param string $purchaseOrderItemUuid
     * @return string
     */
    public function insertStockin(string $productUuid, float $quantity, string $quantityUnitUuid, string $locationCaseUuid, string $stockChargeUuid
        , string                         $serialNo, string $timeApprove, string $userUuid, string $comment
        , string                         $factoryOrderUuid = '', string $purchaseOrderItemUuid = ''): string
    {
        $chargeUuid = '';
        if ($stockChargeUuid && !empty($tmpChargeUuid = $this->stockChargeTable->getStockChargeUuidByChargeCode($stockChargeUuid))) {
            $chargeUuid = $tmpChargeUuid;
        }
        $base = $this->computeQuantityToBase($quantity, $quantityUnitUuid);
        return $this->stockinTable->insertStockin($productUuid, $base['quantity'], $base['uuid'], $locationCaseUuid, $chargeUuid, $serialNo, $timeApprove, $userUuid, $comment, $factoryOrderUuid, $purchaseOrderItemUuid);
    }

    /**
     * If empty(stock_charge_uuid) then it looks with stock_charge_code for it.
     *
     * @param StockinEntity $stockinEntity
     * @return string stockin_uuid
     */
    public function insertStockinWithEntity(StockinEntity $stockinEntity): string
    {
        $base = $this->computeQuantityToBase($stockinEntity->getStockinQuantity(), $stockinEntity->getQuantityunitUuid());
        $stockinEntity->setStockinQuantity($base['quantity']);
        $stockinEntity->setQuantityunitUuid($base['uuid']);
        if (empty($stockinEntity->getStockChargeUuid()) && !empty($stockinEntity->getStockChargeCode())
            && !empty($chargeUuid = $this->stockChargeTable->getStockChargeUuidByChargeCode($stockinEntity->getStockChargeCode()))) {
            $stockinEntity->setStockChargeUuid($chargeUuid);
        }
        return $this->stockinTable->insertStockinWithEntity($stockinEntity);
    }

    /**
     * Converts the $quantity and $quantityUnitUuid to the base size/unit in quantityUnitResolutionGroup.
     * @param float $quantity
     * @param string $quantityUnitUuid
     * @return array ['quantity' => 0.0, 'uuid' => ''] The quantity in the base unit and the UUID from the base unit.
     */
    protected function computeQuantityToBase(float $quantity, string $quantityUnitUuid): array
    {
        $qu = $this->quantityUnitService->getQuantityUnit($quantityUnitUuid);
        if ($qu['quantityunit_resolution'] != 1) {
            $quantity *= $qu['quantityunit_resolution'];
            $quantityUnitUuid = $this->quantityUnitService->getQuantityUnitBaseResolution($quantityUnitUuid)['quantityunit_uuid'];
        }
        return ['quantity' => $quantity, 'uuid' => $quantityUnitUuid];
    }

    /**
     * @param string $stockinUuid
     * @return array From `view_stockin`
     */
    public function getStockin(string $stockinUuid): array
    {
        return $this->viewStockinTable->getStockin($stockinUuid);
    }

    public function getStockChargeUuidAssoc(): array
    {
        return $this->stockChargeTable->getStockChargeUuidAssoc();
    }

    public function searchStockCharge(string $chargeCode): array
    {
        return $this->stockChargeTable->searchStockCharge($chargeCode);
    }

    /**
     * @param string $chargeCode
     * @return bool Also an empty string results in FALSE.
     */
    public function existStockChargeCode(string $chargeCode): bool
    {
        if (empty($chargeCode)) {
            return false;
        }
        return $this->stockChargeTable->existStockChargeCode($chargeCode);
    }

    public function getStockChargeUuidByChargeCode(string $chargeCode): string
    {
        return $this->stockChargeTable->getStockChargeUuidByChargeCode($chargeCode);
    }

    /**
     * @param string $chargeCode
     * @param string $productUuid
     * @return string The existing or the new generated 'stock_charge_uuid'.
     */
    public function checkInsertChargeCode(string $chargeCode, string $productUuid): string
    {
        if (!empty($chargeUuid = $this->getStockChargeUuidByChargeCode($chargeCode))) {
            return $chargeUuid;
        }
        return $this->stockChargeTable->insertStockCharge($chargeCode, $productUuid);
    }

    public function existPairingStockinUuidChargeUuid(string $stockinUuid, string $chargeUuid): bool
    {
        return $this->stockinTable->existPairingStockinUuidChargeUuid($stockinUuid, $chargeUuid);
    }

    public function getStockinUuidForChargeCode(string $chargeCode): string
    {
        $sis = $this->stockinTable->getStockinsByChargeCode($chargeCode);
        if (empty($sis)) {
            return '';
        }
        return $sis[0]['stockin_uuid'];
    }

    public function getStockinUuidForChargeUuid(string $chargeUuid): string
    {
        $sis = $this->stockinTable->getStockinsByChargeUuid($chargeUuid);
        if (empty($sis)) {
            return '';
        }
        return $sis[0]['stockin_uuid'];
    }

    /**
     * Insert stockout without origin (factoryorder | order)
     * @param string $stockinUuid
     * @param float $quantity
     * @param string $comment
     * @param string $userUuid
     * @return string
     */
    public function insertStockoutFree(string $stockinUuid, float $quantity, string $comment, string $userUuid): string
    {
        return $this->stockoutTable->insertStockoutFree($stockinUuid, $quantity, $comment, $userUuid);
    }

    /**
     * Insert stockout with origin (factoryorder | order).
     * @param ParamsStockoutEntity $ppse
     * @return string
     */
    public function insertStockoutEntity(ParamsStockoutEntity $ppse): string
    {
        return $this->stockoutTable->insertStockoutEntity($ppse);
    }

    /**
     * First the function $this->computeQuantityToBase($quantity, $quantityUnitUuid) is called.
     * Then we have the requested size in the base unit.
     * @param string $productUuid
     * @param float $quantity
     * @param string $quantityUnitUuid
     * @return float The difference in the base unit. A negative result shows how much is missing and a positive one what is more than enough in stock.
     */
    public function getStockDifference(string $productUuid, float $quantity, string $quantityUnitUuid): float
    {
        $base = $this->computeQuantityToBase($quantity, $quantityUnitUuid);
        $stock = $this->viewStockinTable->getStockQuantityForProduct($productUuid);
        $quantity = $base['quantity'];
        if ($stock <= 0) {
            return $quantity * -1;
        }
        return $stock - $quantity;
    }

    /**
     * @param string $stockinUuid
     * @param float $qntty In base quantityunit.
     * @return bool
     */
    public function enoughInStock(string $stockinUuid, float $qntty): bool
    {
        $stockin = $this->viewStockinTable->getStockin($stockinUuid);
        if (empty($stockin)) {
            return false;
        }
        return ($stockin['stockin_quantity'] - $stockin['stockout_quantity']) >= $qntty;
    }

    /**
     * @param string $productUuid
     * @param bool $alsoEmpty
     * @return array From db.view_stockin ORDER BY stockin_time_create DESC.
     */
    public function getProductStockins(string $productUuid, bool $alsoEmpty = false): array
    {
        return $this->viewStockinTable->getProductStocks($productUuid, $alsoEmpty);
    }

    /**
     * @param array $productUuids
     * @param string $locationPlaceUuid
     * @return array
     */
    public function getProductsStockinsForLocation(array $productUuids, string $locationPlaceUuid): array
    {
        $stockins = [];
        foreach ($productUuids as $productUuid) {
            $psi = $this->getProductStockinsForLocation($productUuid, $locationPlaceUuid);
            if (empty($psi)) {
                continue;
            }
            $stockins = ArrayUtils::merge($stockins, $psi);
        }
        return $stockins;
    }

    /**
     * @param string $productUuid
     * @param string $locationPlaceUuid
     * @return array From db.view_stockin ORDER BY stockin_time_create DESC.
     */
    public function getProductStockinsForLocation(string $productUuid, string $locationPlaceUuid): array
    {
        return $this->viewStockinTable->getProductStockinsForLocation($productUuid, $locationPlaceUuid);
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function getProductStockouts(string $productUuid): array
    {
        return $this->viewStockoutTable->getProductStockouts($productUuid);
    }

    /**
     * @param string $orderUuid
     * @return array From db.view_stockout.
     */
    public function getOrderStockouts(string $orderUuid): array
    {
        return $this->viewStockoutTable->getOrderStockouts($orderUuid);
    }

    public function getFactoryorderStockouts(string $foUuid): array
    {
        return $this->viewStockoutTable->getFactoryorderStockouts($foUuid);
    }

    /**
     * @param string $productUuid
     * @param string $locationCaseUuid
     * @return bool
     */
    public function existStockinForProductAndLocationCase(string $productUuid, string $locationCaseUuid): bool
    {
        return !empty($this->stockinTable->getStockinsForProductAndLocationCase($productUuid, $locationCaseUuid));
    }

    /**
     * @param ParamsStockinSearchEntity $queryParamsStockinEntity
     * @param bool $doCount
     * @return array From db.view_stockin
     */
    public function searchStockin(ParamsStockinSearchEntity $queryParamsStockinEntity, bool $doCount = false): array
    {
        return $this->viewStockinTable->searchProductStocks($queryParamsStockinEntity, $doCount);
    }

    /**
     * @param string $productUuid
     * @return array From view_product_stock(product_uuid, sum_stockin, sum_stockout).
     */
    public function getViewProductStock(string $productUuid): array
    {
        return $this->viewProductStockTable->getViewProductStock($productUuid);
    }
}
