<?php

namespace Lerp\Stock\Entity;

use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Insert;

class ParamsStockoutEntity extends AbstractParamsStockEntity
{
    protected string $stockinUuid = '';
    protected float $quantity = 0;
    protected string $orderUuid = '';
    protected string $factoryOrderUuid = '';
    protected string $orderItemUuid = '';
    protected string $orderItemListUuid = '';
    protected string $orderItemPartUuid = '';
    protected string $orderItemMaintPartUuid = '';
    protected string $comment = '';
    protected string $userUuid = '';

    public function setStockinUuid(string $stockinUuid): void
    {
        $this->stockinUuid = $stockinUuid;
    }

    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    public function setOrderUuid(string $orderUuid): void
    {
        $this->orderUuid = $orderUuid;
    }

    public function setFactoryOrderUuid(string $factoryOrderUuid): void
    {
        $this->factoryOrderUuid = $factoryOrderUuid;
    }

    public function setOrderItemUuid(string $orderItemUuid): void
    {
        $this->orderItemUuid = $orderItemUuid;
    }

    public function setOrderItemListUuid(string $orderItemListUuid): void
    {
        $this->orderItemListUuid = $orderItemListUuid;
    }

    public function setOrderItemPartUuid(string $orderItemPartUuid): void
    {
        $this->orderItemPartUuid = $orderItemPartUuid;
    }

    public function setOrderItemMaintPartUuid(string $orderItemMaintPartUuid): void
    {
        $this->orderItemMaintPartUuid = $orderItemMaintPartUuid;
    }

    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }

    public function setUserUuid(string $userUuid): void
    {
        $this->userUuid = $userUuid;
    }

    public function getStockinUuid(): string
    {
        return $this->stockinUuid;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setFromParamsArray(array $qp): void
    {
        if (!$this->uuidValid->isValid($qp['stockin_uuid'] ?? '')) {
            $this->success = false;
            $this->addMessage('stockin_uuid is not valid');
            return;
        }
        $this->setStockinUuid($qp['stockin_uuid']);
        $this->setQuantity(floatval(filter_var($qp['quantity'], FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION])));
        if ($this->isNotEmptyAndValidUuid($qp['order_uuid'] ?? '')) {
            $this->setOrderUuid($qp['order_uuid']);
        }
        if ($this->isNotEmptyAndValidUuid($qp['factoryorder_uuid'] ?? '')) {
            $this->setFactoryOrderUuid($qp['factoryorder_uuid']);
        }
        if ($this->isNotEmptyAndValidUuid($qp['order_item_uuid'] ?? '')) {
            $this->setOrderItemUuid($qp['order_item_uuid']);
        }
        if ($this->isNotEmptyAndValidUuid($qp['order_item_list_uuid'] ?? '')) {
            $this->setOrderItemListUuid($qp['order_item_list_uuid']);
        }
        if ($this->isNotEmptyAndValidUuid($qp['order_item_part_uuid'] ?? '')) {
            $this->setOrderItemPartUuid($qp['order_item_part_uuid']);
        }
        if ($this->isNotEmptyAndValidUuid($qp['order_item_maint_part_uuid'] ?? '')) {
            $this->setOrderItemMaintPartUuid($qp['order_item_maint_part_uuid']);
        }
        $this->setComment($this->stringFilter->filter($qp['comment'] ?? ''));
        if (!$this->uuidValid->isValid($qp['user_uuid'] ?? '')) {
            $this->success = false;
            $this->addMessage('user_uuid is not valid');
            return;
        }
        $this->setUserUuid($qp['user_uuid']);
        if (empty($this->factoryOrderUuid) && empty($this->orderUuid)) {
            $this->success = false;
            $this->addMessage('Both, factoryOrderUuid & orderUuid can not be empty');
            return;
        }
        //if (!empty($this->factoryOrderUuid) && empty($this->orderItemListUuid)) {
        //    $this->success = false;
        //    $this->addMessage('If factoryOrderUuid then orderItemListUuid can not be empty');
        //    return;
        //}
        if (!empty($this->orderUuid) && (empty($this->orderItemUuid) && empty($this->orderItemPartUuid) && empty($this->orderItemMaintPartUuid))) {
            $this->success = false;
            $this->addMessage('If orderUuid then one of the following can not be empty: orderItemUuid, orderItemPartUuid, orderItemMaintPartUuid');
            return;
        }
    }

    public function computeInsert(Insert &$insert, string $stockoutUuid): void
    {
        if (!$this->success) {
            return;
        }
        $insert->values([
            'stockout_uuid' => $stockoutUuid
            , 'stockin_uuid' => $this->stockinUuid
            , 'stockout_quantity' => $this->quantity
            , 'order_uuid' => $this->orderUuid ?: null
            , 'factoryorder_uuid' => $this->factoryOrderUuid ?: null
            , 'order_item_uuid' => $this->orderItemUuid ?: null
            , 'order_item_list_uuid' => $this->orderItemListUuid ?: null
            , 'order_item_part_uuid' => $this->orderItemPartUuid ?: null
            , 'order_item_maint_part_uuid' => $this->orderItemMaintPartUuid ?: null
            , 'user_uuid_create' => $this->userUuid
            , 'stockout_comment' => $this->comment
        ]);
    }

}
