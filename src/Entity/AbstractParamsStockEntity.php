<?php

namespace Lerp\Stock\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Validator\Uuid;

abstract class AbstractParamsStockEntity extends ParamsBase
{
    protected DateTimeFormatter $dateFilter; // for search date from & to
    protected string $dateTimeFormat = 'Y-m-d H:i:s.u';
    protected FilterChainStringSanitize $stringFilter;

    public function __construct()
    {
        parent::__construct();
        $this->dateFilter = new DateTimeFormatter();
        $this->dateFilter->setFormat($this->dateTimeFormat);
        $this->stringFilter = new FilterChainStringSanitize();
    }
}
