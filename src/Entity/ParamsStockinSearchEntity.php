<?php

namespace Lerp\Stock\Entity;

use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;

class ParamsStockinSearchEntity extends AbstractParamsStockEntity
{
    protected string $productText;
    protected string $productStructure;
    protected string $placeUuid;
    protected string $chargeCode;
    protected string $factoryorderNo;
    protected string $purchaseOrderNo;
    protected string $timeCreateFrom;
    protected string $timeCreateTo;
    protected string $productNoNo;
    protected bool $onlyWithStock = false;

    protected array $orderFieldsAvailable = [
        'stockin_time_create',
        'stock_charge_code',
        'product_no_no',
        'product_text_part',
        'product_text_short',
        'factoryorder_no',
        'purchase_order_no',
    ];

    public function setProductText(string $productText): void
    {
        $this->productText = $productText;
    }

    public function setProductStructure(string $productStructure): void
    {
        $this->productStructure = $productStructure;
    }

    public function setPlaceUuid(string $placeUuid): void
    {
        $this->placeUuid = $placeUuid;
    }

    public function setChargeCode(string $chargeCode): void
    {
        $this->chargeCode = $chargeCode;
    }

    public function setFactoryorderNo(string $factoryorderNo): void
    {
        $this->factoryorderNo = $factoryorderNo;
    }

    public function setPurchaseOrderNo(string $purchaseOrderNo): void
    {
        $this->purchaseOrderNo = $purchaseOrderNo;
    }

    public function setProductNoNo(string $productNoNo): void
    {
        $this->productNoNo = $productNoNo;
    }

    public function setTimeCreateFrom(string $timeCreateFrom): void
    {
        $time = $this->dateFilter->filter($timeCreateFrom);
        if ($time instanceof \DateTime) {
            $time = $time->format($this->dateTimeFormat);
        }
        if (!is_string($time)) {
            return;
        }
        $this->timeCreateFrom = $time;
    }

    public function setTimeCreateTo(string $timeCreateTo): void
    {
        $time = $this->dateFilter->filter($timeCreateTo);
        if ($time instanceof \DateTime) {
            $time = $time->format($this->dateTimeFormat);
        }
        if (!is_string($time)) {
            return;
        }
        $this->timeCreateTo = $time;
    }

    public function setOnlyWithStock(bool $onlyWithStock): void
    {
        $this->onlyWithStock = $onlyWithStock;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setProductText($this->stringFilter->filter($qp['product_text']));
        $this->setProductStructure($this->stringFilter->filter($qp['product_structure']));
        $this->setPlaceUuid(intval($qp['place_uuid'] ?? 0));
        $this->setChargeCode($this->stringFilter->filter($qp['charge_code'] ?? ''));
        $this->setFactoryorderNo($this->stringFilter->filter($qp['factoryorder_no'] ?? ''));
        $this->setPurchaseOrderNo($this->stringFilter->filter($qp['purchase_order_no'] ?? ''));
        $this->setProductNoNo($this->stringFilter->filter($qp['product_no_no'] ?? ''));
        $this->setTimeCreateFrom($qp['time_create_from'] ?? '');
        $this->setTimeCreateTo($qp['time_create_to'] ?? '');
        $this->setOnlyWithStock(filter_var($qp['only_with_stock'], FILTER_VALIDATE_BOOL));
    }

    /**
     * Compute the Select for db.view_stockin.
     *
     * @param Select $select
     * @param string $orderDefault
     * @return void
     */
    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        parent::computeSelect($select);
        if (!empty($this->productText)) {
            $select->where->NEST->like('product_text_part', '%' . $this->productText . '%')
                ->OR->like('product_text_short', '%' . $this->productText . '%')
                ->OR->like('product_text_long', '%' . $this->productText . '%');
        }
        if (!empty($this->productStructure)) {
            $select->where(['product_structure' => $this->productStructure]);
        }
        if (!empty($this->placeUuid)) {
            $select->where(['location_place_uuid' => $this->placeUuid]);
        }
        if (!empty($this->chargeCode)) {
            $select->where->like('stock_charge_code', '%' . $this->chargeCode . '%');
        }
        if (!empty($this->factoryorderNo)) {
            $select->where->like(new Expression('CAST(factoryorder_no AS TEXT)'), '%' . $this->factoryorderNo . '%');
        }
        if (!empty($this->purchaseOrderNo)) {
            $select->where->like(new Expression('CAST(purchase_order_no AS TEXT)'), '%' . $this->purchaseOrderNo . '%');
        }
        if (!empty($this->productNoNo)) {
            $select->where->like(new Expression('CAST(product_no_no AS TEXT)'), '%' . $this->productNoNo . '%');
        }
        if (!empty($this->timeCreateFrom)) {
            $select->where->greaterThanOrEqualTo('stockin_time_create', $this->timeCreateFrom);
        }
        if (!empty($this->timeCreateTo)) {
            $select->where->lessThanOrEqualTo('stockin_time_create', $this->timeCreateTo);
        }
        if ($this->onlyWithStock) {
            $select->where(new Expression('(stockin_quantity - stockout_quantity) > 0'));
        }
    }
}
