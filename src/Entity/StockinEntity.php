<?php

namespace Lerp\Stock\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class StockinEntity extends AbstractEntity
{
    public array $mapping = [
        'stockin_uuid'             => 'stockin_uuid',
        'stockin_code'             => 'stockin_code',
        'product_uuid'             => 'product_uuid',
        'stockin_quantity'         => 'stockin_quantity',
        'quantityunit_uuid'        => 'quantityunit_uuid',
        'stockin_charge_supplier'  => 'stockin_charge_supplier',
        'stock_charge_uuid'        => 'stock_charge_uuid',
        'stock_charge_code'        => 'stock_charge_code', // db.stock_charge.stock_charge_code
        'factoryorder_uuid'        => 'factoryorder_uuid',
        'purchase_order_item_uuid' => 'purchase_order_item_uuid',
        'stockin_serial_no'        => 'stockin_serial_no',
        'stockin_time_create'      => 'stockin_time_create',
        'stockin_time_approve'     => 'stockin_time_approve',
        'user_uuid_create'         => 'user_uuid_create',
        'stockin_comment'          => 'stockin_comment',
        'location_case_uuid'       => 'location_case_uuid',
    ];

    public function getStockinUuid(): string
    {
        if (!isset($this->storage['stockin_uuid'])) {
            return '';
        }
        return $this->storage['stockin_uuid'];
    }

    public function setStockinUuid(string $stockinUuid): void
    {
        $this->storage['stockin_uuid'] = $stockinUuid;
    }

    public function getStockinCode(): string
    {
        if (!isset($this->storage['stockin_code'])) {
            return '';
        }
        return $this->storage['stockin_code'];
    }

    public function setStockinCode(string $stockinCode): void
    {
        $this->storage['stockin_code'] = $stockinCode;
    }

    public function getProductUuid(): string
    {
        if (!isset($this->storage['product_uuid'])) {
            return '';
        }
        return $this->storage['product_uuid'];
    }

    public function setProductUuid(string $productUuid): void
    {
        $this->storage['product_uuid'] = $productUuid;
    }

    public function getStockinQuantity(): float
    {
        if (!isset($this->storage['stockin_quantity'])) {
            return 0;
        }
        return $this->storage['stockin_quantity'];
    }

    public function setStockinQuantity(float $stockinQuantity): void
    {
        $this->storage['stockin_quantity'] = $stockinQuantity;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getStockChargeSupplier(): string
    {
        if (!isset($this->storage['stockin_charge_supplier'])) {
            return '';
        }
        return $this->storage['stockin_charge_supplier'];
    }

    public function setStockChargeSupplier(string $stockChargeSupplier): void
    {
        $this->storage['stockin_charge_supplier'] = $stockChargeSupplier;
    }

    public function getStockChargeUuid(): string
    {
        if (!isset($this->storage['stock_charge_uuid'])) {
            return '';
        }
        return $this->storage['stock_charge_uuid'];
    }

    public function setStockChargeUuid(string $stockChargeUuid): void
    {
        $this->storage['stock_charge_uuid'] = $stockChargeUuid;
    }

    public function getStockChargeCode(): string
    {
        if (!isset($this->storage['stock_charge_code'])) {
            return '';
        }
        return $this->storage['stock_charge_code'];
    }

    public function setStockChargeCode(string $stockChargeCode): void
    {
        $this->storage['stock_charge_code'] = $stockChargeCode;
    }

    public function getFactoryorderUuid(): string
    {
        if (!isset($this->storage['factoryorder_uuid'])) {
            return '';
        }
        return $this->storage['factoryorder_uuid'];
    }

    public function setFactoryorderUuid(string $factoryorderUuid): void
    {
        $this->storage['factoryorder_uuid'] = $factoryorderUuid;
    }

    public function getPurchaseOrderItemUuid(): string
    {
        if (!isset($this->storage['purchase_order_item_uuid'])) {
            return '';
        }
        return $this->storage['purchase_order_item_uuid'];
    }

    public function setPurchaseOrderItemUuid(string $purchaseOrderItemUuid): void
    {
        $this->storage['purchase_order_item_uuid'] = $purchaseOrderItemUuid;
    }

    public function getStockinSerialNo(): string
    {
        if (!isset($this->storage['stockin_serial_no'])) {
            return '';
        }
        return $this->storage['stockin_serial_no'];
    }

    public function setStockinSerialNo(string $stockinSerialNo): void
    {
        $this->storage['stockin_serial_no'] = $stockinSerialNo;
    }

    public function getStockinTimeCreate(): string
    {
        if (!isset($this->storage['stockin_time_create'])) {
            return '';
        }
        return $this->storage['stockin_time_create'];
    }

    public function setStockinTimeCreate(string $stockinTimeCreate): void
    {
        $this->storage['stockin_time_create'] = $stockinTimeCreate;
    }

    public function getStockinTimeApprove(): string
    {
        if (!isset($this->storage['stockin_time_approve'])) {
            return '';
        }
        return $this->storage['stockin_time_approve'];
    }

    public function setStockinTimeApprove(string $stockinTimeApprove): void
    {
        $this->storage['stockin_time_approve'] = $stockinTimeApprove;
    }

    public function getUserUuidCreate(): string
    {
        if (!isset($this->storage['user_uuid_create'])) {
            return '';
        }
        return $this->storage['user_uuid_create'];
    }

    public function setUserUuidCreate(string $userUuidCreate): void
    {
        $this->storage['user_uuid_create'] = $userUuidCreate;
    }

    public function getStockinComment(): string
    {
        if (!isset($this->storage['stockin_comment'])) {
            return '';
        }
        return $this->storage['stockin_comment'];
    }

    public function setStockinComment(string $stockinComment): void
    {
        $this->storage['stockin_comment'] = $stockinComment;
    }

    public function getLocationCaseUuid(): string
    {
        if (!isset($this->storage['location_case_uuid'])) {
            return '';
        }
        return $this->storage['location_case_uuid'];
    }

    public function setLocationCaseUuid(string $locationCaseUuid): void
    {
        $this->storage['location_case_uuid'] = $locationCaseUuid;
    }
}
