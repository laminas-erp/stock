<?php

namespace Lerp\Stock\Factory\Controller\Ajax;

use Bitkorn\User\Service\UserService;
use Interop\Container\Containerinterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Product\Service\ProductService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Stock\Controller\Ajax\StockinAjaxController;
use Lerp\Stock\Form\StockinForm;
use Lerp\Stock\Service\StockService;

class StockinAjaxControllerFactory implements FactoryInterface
{
    public function __invoke(Containerinterface $container, $requestedName, array $options = null)
    {
        $controller = new StockinAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setStockinForm($container->get(StockinForm::class));
        $controller->setStockService($container->get(StockService::class));
        $controller->setProductService($container->get(ProductService::class));
        $controller->setPurchaseOrderItemService($container->get(PurchaseOrderItemService::class));
        return $controller;
    }
}