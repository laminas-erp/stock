<?php

namespace Lerp\Stock\Factory\Service;

use Interop\Container\ContainerInterface;
use Lerp\Common\Service\Lists\QuantityUnitService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Stock\Service\StockService;
use Lerp\Stock\Table\StockChargeTable;
use Lerp\Stock\Table\StockinTable;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Stock\Table\StockoutTable;
use Lerp\Stock\Table\ViewProductStockTable;
use Lerp\Stock\Table\ViewStockChargeTable;
use Lerp\Stock\Table\ViewStockinTable;
use Lerp\Stock\Table\ViewStockoutTable;

class StockServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): StockService
    {
        $service = new StockService();
        $service->setLogger($container->get('logger'));
        $service->setStockinTable($container->get(StockinTable::class));
        $service->setStockoutTable($container->get(StockoutTable::class));
        $service->setViewStockinTable($container->get(ViewStockinTable::class));
        $service->setViewStockoutTable($container->get(ViewStockoutTable::class));
        $service->setViewProductStockTable($container->get(ViewProductStockTable::class));
        $service->setStockChargeTable($container->get(StockChargeTable::class));
        $service->setViewStockChargeTable($container->get(ViewStockChargeTable::class));
        $service->setQuantityUnitService($container->get(QuantityUnitService::class));
        return $service;
    }
}
