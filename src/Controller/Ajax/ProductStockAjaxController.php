<?php

namespace Lerp\Stock\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Product\Service\ProductService;
use Lerp\Stock\Service\StockService;

class ProductStockAjaxController extends AbstractUserController
{
    protected StockService $stockService;
    protected ProductService $productService;

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    /**
     * Stockin view for a product.
     *
     * @return JsonModel
     */
    public function stockinViewAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = $this->params('product_uuid');
        $alsoEmpty = $this->params('also_empty') == 1;
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->stockService->getProductStockins($productUuid, $alsoEmpty));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Stockin view for a product in a location (location_place_uuid).
     *
     * @return JsonModel
     */
    public function stockinViewLocationAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $productUuids = $this->getRequest()->getPost('product_uuid');
        $locationPlaceUuid = filter_var($this->getRequest()->getPost('location_place_uuid'), FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uuidV = new Uuid();
        if (empty($productUuids) || !is_array($productUuids) || empty($locationPlaceUuid) || !$uuidV->isValid($locationPlaceUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        foreach ($productUuids as $productUuid) {
            $productUuid = filter_var($productUuid, FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
            if (!$uuidV->isValid($productUuid)) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                $jsonModel->addMessage('Product UUID not valid: ' . $productUuid);
                return $jsonModel;
            }
        }
        $jsonModel->setArr($this->stockService->getProductsStockinsForLocation($productUuids, $locationPlaceUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Stockout view for a product.
     *
     * @return JsonModel
     */
    public function stockoutViewAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = $this->params('product_uuid');
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($productStocks = $this->stockService->getProductStockouts($productUuid))) {
            $jsonModel->setArr($productStocks);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * Stock in & out with product_uud computed for a product.
     *
     * @return JsonModel
     */
    public function stockSummaryAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = $this->params('product_uuid');
        if (!(new Uuid())->isValid($productUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($productStock = $this->stockService->getViewProductStock($productUuid))) {
            $jsonModel->setObj($productStock);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
