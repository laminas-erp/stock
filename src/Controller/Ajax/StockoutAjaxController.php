<?php

namespace Lerp\Stock\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Stock\Entity\ParamsStockoutEntity;
use Lerp\Stock\Service\StockService;

class StockoutAjaxController extends AbstractUserController
{
    protected StockService $stockService;

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    /**
     * @return JsonModel
     */
    public function createStockoutAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $data = $this->getRequest()->getPost()->toArray();
        $data['user_uuid'] = $this->userService->getUserUuid();
        $pp = new ParamsStockoutEntity();
        $pp->setFromParamsArray($data);
        if(!$pp->isSuccess()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            $jsonModel->addMessages($pp->getMessages());
            return $jsonModel;
        }
        if(!$this->stockService->enoughInStock($pp->getStockinUuid(), $pp->getQuantity())) {
            $jsonModel->addMessage('Der Lagerbestand ist nicht ausreichend!');
            return $jsonModel;
        }
        if (!empty($uuid = $this->stockService->insertStockoutEntity($pp))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setUuid($uuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * Insert multiple stockouts without origin (factoryorder | order etc).
     * POST array: key=stockin_uuid; value=quantity
     * POST array rest: key=comment; value='' (string)
     * @return JsonModel
     */
    public function createStockoutsFreeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $comment = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH], 'options' => ['default' => '']]);
        $uv = new Uuid();
        $post = $request->getPost()->toArray();
        $values = [];
        foreach ($post as $key => $value) {
            $value = floatval($value);;
            if (!$uv->isValid($key) || empty($value)) {
                continue;
            }
            $values[$key] = $value;
        }
        if (count($values) < 1) {
            return $jsonModel;
        }
        $success = true;
        foreach ($values as $uuid => $qntty) {
            if (!$this->stockService->enoughInStock($uuid, $qntty)) {
                $jsonModel->addMessage('Nicht genug im Lager');
                $success = false;
                continue;
            }
            if (!$this->stockService->insertStockoutFree($uuid, $qntty, $comment, $this->userService->getUserUuid())) {
                return $jsonModel;
            }
        }
        if ($success) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function outsForOrderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isGet()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $orderUuid = $this->params('order_uuid');
        if (!((new Uuid())->isValid($orderUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->stockService->getOrderStockouts($orderUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function outsForFactoryorderAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isGet()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_405);
            return $jsonModel;
        }
        $foUuid = $this->params('fo_uuid');
        if (!((new Uuid())->isValid($foUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->stockService->getFactoryorderStockouts($foUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
