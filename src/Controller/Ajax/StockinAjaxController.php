<?php

namespace Lerp\Stock\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Product\Service\ProductService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderItemService;
use Lerp\Stock\Entity\ParamsStockinSearchEntity;
use Lerp\Stock\Entity\StockinEntity;
use Lerp\Stock\Form\StockinForm;
use Lerp\Stock\Service\StockService;

class StockinAjaxController extends AbstractUserController
{
    protected StockinForm $stockinForm;
    protected StockService $stockService;
    protected ProductService $productService;
    protected PurchaseOrderItemService $purchaseOrderItemService;

    public function setStockinForm(StockinForm $stockinForm): void
    {
        $this->stockinForm = $stockinForm;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    public function setProductService(ProductService $productService): void
    {
        $this->productService = $productService;
    }

    public function setPurchaseOrderItemService(PurchaseOrderItemService $purchaseOrderItemService): void
    {
        $this->purchaseOrderItemService = $purchaseOrderItemService;
    }

    /**
     * Create a stockin WITHOUT factoryorder | purchase.
     *
     * @return JsonModel
     */
    public function createStockinFreeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $productUuid = filter_input(INPUT_POST, 'product_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $quantity = floatval(filter_input(INPUT_POST, 'stockin_quantity', FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => FILTER_FLAG_ALLOW_FRACTION]));
        $quUuid = filter_input(INPUT_POST, 'quantityunit_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $caseUuid = filter_input(INPUT_POST, 'location_case_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $serialNo = filter_input(INPUT_POST, 'stockin_serial_no', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]], ['options' => ['default' => '']]);
        $timeApprove = filter_input(INPUT_POST, 'stockin_time_approve', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]], ['options' => ['default' => '']]);
        $comment = filter_input(INPUT_POST, 'stockin_comment', FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $uv = new Uuid();
        if (
            !$uv->isValid($productUuid)
            || empty($quantity) || $quantity < 0 || !$uv->isValid($quUuid) || !$uv->isValid($caseUuid)
            || !$this->productService->hasProductLocationCaseUuid($productUuid, $caseUuid)
            || !$this->productService->hasProductQuantityunit($productUuid, $quUuid)
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($timeApprove)) {
            $timeApprove = $this->stockService->formatDateString($timeApprove);
        }
        if (!empty($stockinUuid = $this->stockService->insertStockin($productUuid, $quantity, $quUuid, $caseUuid
            , '', $serialNo, $timeApprove, $this->userService->getUserUuid(), $comment))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($stockinUuid);
        }

        return $jsonModel;
    }

    /**
     * Create a stock-in.
     *
     * @return JsonModel
     */
    public function createStockinAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($postArray = $this->getRequest()->getPost()->toArray())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $postArray['user_uuid_create'] = $this->userService->getUserUuid();
        $this->stockinForm->setStockinOrigin(StockService::STOCKIN_ORIGIN_PURCHASEORDER);
        $this->stockinForm->init();
        $this->stockinForm->setData($postArray);
        if (!$this->stockinForm->isValid()) {
            $jsonModel->addMessages($this->stockinForm->getMessages());
            return $jsonModel;
        }
        $stockinEntity = new StockinEntity();
        if (!$stockinEntity->exchangeArrayFromDatabase($this->stockinForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!$this->productService->hasProductLocationCaseUuid($stockinEntity->getProductUuid(), $stockinEntity->getLocationCaseUuid())) {
            $jsonModel->addMessage('location_case_uuid|#|Der Lagerplatz gehört nicht zum Produkt.');
        }
        $stockinEntity->setStockinTimeApprove($this->stockService->formatDateString($stockinEntity->getStockinTimeApprove()));
        if (!empty($stockinUuid = $this->stockService->insertStockinWithEntity($stockinEntity))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($stockinUuid);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function searchStockinsAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $qp = new ParamsStockinSearchEntity();
        $qp->setFromParamsArray($this->params()->fromQuery());
        if (!empty($arr = $this->stockService->searchStockin($qp))) {
            $jsonModel->setArr($arr);
            $jsonModel->setCount($this->stockService->searchStockin($qp, true)[0]);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}