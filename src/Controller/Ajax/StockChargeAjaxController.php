<?php

namespace Lerp\Stock\Controller\Ajax;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Stock\Service\StockService;

class StockChargeAjaxController extends AbstractUserController
{
    protected StockService $stockService;

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    /**
     * @return JsonModel
     */
    public function stockChargeUuidAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->stockService->getStockChargeUuidAssoc());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function searchStockChargeAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setArr($this->stockService->searchStockCharge($this->params('charge', '')));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
