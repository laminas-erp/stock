<?php

namespace Lerp\Stock\Form;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\Digits;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Date;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;
use Lerp\Stock\Service\StockService;

class StockinForm extends AbstractForm implements InputFilterProviderInterface
{
    protected Adapter $dbAdapter;
    protected int $stockinOrigin;

    public function setDbAdapter(Adapter $dbAdapter): void
    {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @param int $stockinOrigin One of the constants StockService::STOCKIN_ORIGIN_FACTORYORDER or StockService::STOCKIN_ORIGIN_PURCHASEORDER
     * @return void
     */
    public function setStockinOrigin(int $stockinOrigin): void
    {
        if (!in_array($stockinOrigin, array_keys(StockService::$stockinOrigins))) {
            return;
        }
        $this->stockinOrigin = $stockinOrigin;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'stockin_uuid']);
        }
        $this->add(['name' => 'product_uuid']);
        $this->add(['name' => 'stockin_quantity']);
        $this->add(['name' => 'quantityunit_uuid']);
        $this->add(['name' => 'stockin_charge_supplier']);
        $this->add(['name' => 'stock_charge_uuid']);
        $this->add(['name' => 'stock_charge_code']);
        switch ($this->stockinOrigin) {
            case StockService::STOCKIN_ORIGIN_FACTORYORDER:
                $this->add(['name' => 'factoryorder_uuid']);
                break;
            case StockService::STOCKIN_ORIGIN_PURCHASEORDER:
                $this->add(['name' => 'purchase_order_item_uuid']);
                break;
        }
        $this->add(['name' => 'stockin_serial_no']);
        $this->add(['name' => 'stockin_time_approve']);
        $this->add(['name' => 'user_uuid_create']);
        $this->add(['name' => 'stockin_comment']);
        $this->add(['name' => 'location_case_uuid']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['stockin_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['product_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class]
            ]
        ];

        $filter['stockin_quantity'] = [
            'required'   => true,
            'filters'    => [
                ['name' => Digits::class]
            ],
            'validators' => [
                [
                    'name'    => NotEmpty::class,
                    'options' => [
                        'type' => NotEmpty::ALL
                    ]
                ]
            ]
        ];

        $filter['quantityunit_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class]
            ]
        ];

        $filter['stockin_charge_supplier'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 180
                    ]
                ]
            ]
        ];

        $filter['stock_charge_uuid'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->dbAdapter,
                        'table'   => 'stock_charge',
                        'field'   => 'stock_charge_uuid',
                    ]
                ]
            ]
        ];

        $filter['stock_charge_code'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 30,
                    ]
                ]
            ]
        ];

        switch ($this->stockinOrigin) {
            case StockService::STOCKIN_ORIGIN_FACTORYORDER:
                $filter['factoryorder_uuid'] = [
                    'required'      => true,
                    'filters'       => [
                        ['name' => SanitizeStringFilter::class],
                    ], 'validators' => [
                        ['name' => Uuid::class],
                        [
                            'name'    => RecordExists::class,
                            'options' => [
                                'adapter' => $this->dbAdapter,
                                'table'   => 'factoryorder',
                                'field'   => 'factoryorder_uuid',
                            ]
                        ]
                    ]
                ];
                break;
            case StockService::STOCKIN_ORIGIN_PURCHASEORDER:
                $filter['purchase_order_item_uuid'] = [
                    'required'      => true,
                    'filters'       => [
                        ['name' => SanitizeStringFilter::class],
                    ], 'validators' => [
                        ['name' => Uuid::class],
                        [
                            'name'    => RecordExists::class,
                            'options' => [
                                'adapter' => $this->dbAdapter,
                                'table'   => 'purchase_order_item',
                                'field'   => 'purchase_order_item_uuid',
                            ]
                        ]
                    ]
                ];
                break;
        }

        $filter['stockin_serial_no'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 80
                    ]
                ]
            ]
        ];

        $filter['stockin_time_approve'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => Date::class,
                    'options' => [
                        'format' => Date::FORMAT_DEFAULT
                    ]
                ]
            ]
        ];

        $filter['user_uuid_create'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->dbAdapter,
                        'table'   => 'user',
                        'field'   => 'user_uuid',
                    ]
                ]
            ]
        ];

        $filter['stockin_comment'] = [
            'required'      => false,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'max' => 40000
                    ]
                ]
            ]
        ];

        $filter['location_case_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class],
                [
                    'name'    => RecordExists::class,
                    'options' => [
                        'adapter' => $this->dbAdapter,
                        'table'   => 'location_case',
                        'field'   => 'location_case_uuid',
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
