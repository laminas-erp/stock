<?php

namespace Lerp\Stock;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Lerp\Stock\Controller\Ajax\ProductStockAjaxController;
use Lerp\Stock\Controller\Ajax\StockChargeAjaxController;
use Lerp\Stock\Controller\Ajax\StockinAjaxController;
use Lerp\Stock\Controller\Ajax\StockoutAjaxController;
use Lerp\Stock\Factory\Controller\Ajax\ProductStockAjaxControllerFactory;
use Lerp\Stock\Factory\Controller\Ajax\StockChargeAjaxControllerFactory;
use Lerp\Stock\Factory\Controller\Ajax\StockinAjaxControllerFactory;
use Lerp\Stock\Factory\Controller\Ajax\StockoutAjaxControllerFactory;
use Lerp\Stock\Factory\Form\StockinFormFactory;
use Lerp\Stock\Factory\Service\StockGodServiceFactory;
use Lerp\Stock\Factory\Service\StockServiceFactory;
use Lerp\Stock\Factory\Table\StockChargeTableFactory;
use Lerp\Stock\Factory\Table\StockinTableFactory;
use Lerp\Stock\Factory\Table\StockoutTableFactory;
use Lerp\Stock\Factory\Table\ViewProductStockTableFactory;
use Lerp\Stock\Factory\Table\ViewStockChargeTableFactory;
use Lerp\Stock\Factory\Table\ViewStockinTableFactory;
use Lerp\Stock\Factory\Table\ViewStockoutTableFactory;
use Lerp\Stock\Form\StockinForm;
use Lerp\Stock\Service\StockGodService;
use Lerp\Stock\Service\StockService;
use Lerp\Stock\Table\StockChargeTable;
use Lerp\Stock\Table\StockinTable;
use Lerp\Stock\Table\StockoutTable;
use Lerp\Stock\Table\ViewProductStockTable;
use Lerp\Stock\Table\ViewStockChargeTable;
use Lerp\Stock\Table\ViewStockinTable;
use Lerp\Stock\Table\ViewStockoutTable;

return [
    'router'                        => [
        'routes' => [
            'lerp_stock_ajax_stockin_create_free'              => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stock-in-create-free',
                    'defaults' => [
                        'controller' => StockinAjaxController::class,
                        'action'     => 'createStockinFree'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockin_create'                   => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stock-in-create',
                    'defaults' => [
                        'controller' => StockinAjaxController::class,
                        'action'     => 'createStockin'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockin_search'                   => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stockin-search',
                    'defaults' => [
                        'controller' => StockinAjaxController::class,
                        'action'     => 'searchStockins'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockout_createout'               => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stock-out-create',
                    'defaults' => [
                        'controller' => StockoutAjaxController::class,
                        'action'     => 'createStockout'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockout_createoutsfree'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stock-outs-create-free',
                    'defaults' => [
                        'controller' => StockoutAjaxController::class,
                        'action'     => 'createStockoutsFree'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockout_fororder'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-stock-outs-for-order/:order_uuid',
                    'constraints' => [
                        'order_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => StockoutAjaxController::class,
                        'action'     => 'outsForOrder'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockout_forfactoryorder'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-stock-outs-for-factoryorder/:fo_uuid',
                    'constraints' => [
                        'fo_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => StockoutAjaxController::class,
                        'action'     => 'outsForFactoryorder'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockcharge_chargeassoc'          => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stock-ajax-list-chargeassoc',
                    'defaults' => [
                        'controller' => StockChargeAjaxController::class,
                        'action'     => 'stockChargeUuidAssoc'
                    ],
                ],
            ],
            'lerp_stock_ajax_stockcharge_searchcharge'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-stock-ajax-search-charge[/:charge]',
                    'constraints' => [
                        'charge' => '[0-9A-Za-z:/()_-]+',
                    ],
                    'defaults'    => [
                        'controller' => StockChargeAjaxController::class,
                        'action'     => 'searchStockCharge'
                    ],
                ],
            ],
            'lerp_stock_ajax_productstock_stockinview'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-stock-ajax-product-stockinview[/:product_uuid[/:also_empty]]',
                    'constraints' => [
                        'product_uuid' => '[0-9A-Fa-f-]+',
                        'also_empty'   => '[0-1]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductStockAjaxController::class,
                        'action'     => 'stockinView'
                    ],
                ],
            ],
            'lerp_stock_ajax_productstock_stockinviewlocation' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/lerp-stock-ajax-product-stockinview-location',
                    'defaults' => [
                        'controller' => ProductStockAjaxController::class,
                        'action'     => 'stockinViewLocation'
                    ],
                ],
            ],
            'lerp_stock_ajax_productstock_stockoutview'        => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-stock-ajax-product-stockoutview[/:product_uuid]',
                    'constraints' => [
                        'product_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductStockAjaxController::class,
                        'action'     => 'stockoutView'
                    ],
                ],
            ],
            'lerp_stock_ajax_productstock_summary'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-stock-ajax-product-summary/:product_uuid',
                    'constraints' => [
                        'product_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ProductStockAjaxController::class,
                        'action'     => 'stockSummary'
                    ],
                ],
            ],
        ],
    ],
    'controllers'                   => [
        'factories' => [
            StockinAjaxController::class      => StockinAjaxControllerFactory::class,
            StockoutAjaxController::class     => StockoutAjaxControllerFactory::class,
            ProductStockAjaxController::class => ProductStockAjaxControllerFactory::class,
            StockChargeAjaxController::class  => StockChargeAjaxControllerFactory::class,
        ]
    ],
    'service_manager'               => [
        'invokables' => [
        ],
        'factories'  => [
            // table
            StockinTable::class          => StockinTableFactory::class,
            StockoutTable::class         => StockoutTableFactory::class,
            ViewStockinTable::class      => ViewStockinTableFactory::class,
            ViewStockoutTable::class     => ViewStockoutTableFactory::class,
            ViewProductStockTable::class => ViewProductStockTableFactory::class,
            StockChargeTable::class      => StockChargeTableFactory::class,
            ViewStockChargeTable::class  => ViewStockChargeTableFactory::class,
            // service
            StockGodService::class       => StockGodServiceFactory::class,
            StockService::class          => StockServiceFactory::class,
            // form
            StockinForm::class           => StockinFormFactory::class,
        ]
    ],
    'view_manager'                  => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_files_category_brands' => [
        /**
         * Each module can have its own category brands for table 'file_category_rel'.
         * The key is known in the module.
         */
        'lerp_stock_stockin'  => 'stockin',
        'lerp_stock_stockout' => 'stockout',
    ],
    'lerp_stock'                    => [
        'module_brand' => 'stock',
    ],
];
